# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Website for Bradco-WISP.
* 1.0

### Set up ###

.less files can be read in a text editor (you can set an editor like Sublime Text or Brackets to display it with CSS syntax, they are basically the same). To make changes to the .less files, however, requires a compiler - I use Prepros on a Mac (it is available for Windows and Linux also).

If you don't want to run a compiler or can't run one, make changes to the styling in a separate custom.css file, and add it into the HTML *after* the Bootstrap stylesheet in the <head>.

Leave comments on additions or proposed changes in both HTML or stylesheet where applicable.

### Who do I talk to? ###

* Repo owner or admin